//
//  ContentView.swift
//  sampleSwiftUI
//
//  Created by Slava Starovoitov on 01.04.2022.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        LandmarkList()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
            .environmentObject(ModelData())
    }
}
