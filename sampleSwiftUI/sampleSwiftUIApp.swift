//
//  sampleSwiftUIApp.swift
//  sampleSwiftUI
//
//  Created by Slava Starovoitov on 01.04.2022.
//

import SwiftUI

@main
struct sampleSwiftUIApp: App {
    @StateObject private var modelData = ModelData()
    
    var body: some Scene {
        WindowGroup {
            ContentView()
                .environmentObject(modelData)
        }
    }
}
